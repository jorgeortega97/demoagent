package com.wposs.myapplication.data;

import android.os.Parcel;

public class data {
    private String id;
    private String url;
    private String dns;
    private String port_dwl;
    private String port_upl;
    private String frequency;
    private String query;
    private String no_query;
    private String init;
    private String time_out_slide;
    private String name_terminal;
    private String is_tls;

   public data(){

   }

    public data(String id, String url, String dns, String port_dwl, String port_upl, String frequency, String query, String no_query, String init, String time_out_slide, String name_terminal, String is_tls) {
        this.id = id;
        this.url = url;
        this.dns = dns;
        this.port_dwl = port_dwl;
        this.port_upl = port_upl;
        this.frequency = frequency;
        this.query = query;
        this.no_query = no_query;
        this.init = init;
        this.time_out_slide = time_out_slide;
        this.name_terminal = name_terminal;
        this.is_tls = is_tls;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDns() {
        return dns;
    }

    public void setDns(String dns) {
        this.dns = dns;
    }

    public String getPort_dwl() {
        return port_dwl;
    }

    public void setPort_dwl(String port_dwl) {
        this.port_dwl = port_dwl;
    }

    public String getPort_upl() {
        return port_upl;
    }

    public void setPort_upl(String port_upl) {
        this.port_upl = port_upl;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getNo_query() {
        return no_query;
    }

    public void setNo_query(String no_query) {
        this.no_query = no_query;
    }

    public String getInit() {
        return init;
    }

    public void setInit(String init) {
        this.init = init;
    }

    public String getTime_out_slide() {
        return time_out_slide;
    }

    public void setTime_out_slide(String time_out_slide) {
        this.time_out_slide = time_out_slide;
    }

    public String getName_terminal() {
        return name_terminal;
    }

    public void setName_terminal(String name_terminal) {
        this.name_terminal = name_terminal;
    }

    public String getIs_tls() {
        return is_tls;
    }

    public void setIs_tls(String is_tls) {
        this.is_tls = is_tls;
    }
}
