package com.wposs.myapplication.content;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class CompanyProviderPackage extends ContentProvider {

    SQLiteDatabase myDB;

    public CompanyProviderPackage() {
    }

    public static final String AUTHORITY = "com.wposs.myapplication.content.CompanyProviderPackage";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/config");

    static int CONFIG = 1;
    static int CONFIG_ID = 2;

    static UriMatcher myUri = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        myUri.addURI(AUTHORITY, "package", CONFIG);
        myUri.addURI(AUTHORITY, "package/#", CONFIG_ID);
    }

    private class MyDatabase extends SQLiteOpenHelper {
        private static final String DB_NAME = "package.db";
        private static final String DB_TABLE = "package";
        private static final int DB_VERSION = 1;

        public MyDatabase(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.execSQL("create table " + DB_TABLE + " (_id integer primary key autoincrement, packageName, status);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
            sqLiteDatabase.execSQL("drop table if exists " + DB_TABLE);
        }
    }

    @Override
    public boolean onCreate() {

        MyDatabase myHelper = new MyDatabase(getContext());

        myDB = myHelper.getWritableDatabase();
        if (myDB != null) {
            return true;
        } else {
            return false;
        }
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] strings, @Nullable String s, @Nullable String[] strings1, @Nullable String s1) {
        SQLiteQueryBuilder myQuery = new SQLiteQueryBuilder();
        myQuery.setTables(MyDatabase.DB_TABLE);

        Cursor cr = myQuery.query(myDB, null, null, null, null, null, "_id");
        cr.setNotificationUri(getContext().getContentResolver(), uri);
        return cr;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        long row = myDB.insert(MyDatabase.DB_TABLE, null, contentValues);

        if (row > 0) {
            uri = ContentUris.withAppendedId(CONTENT_URI, row);
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return uri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {

        int count=myDB.update(MyDatabase.DB_TABLE,contentValues,s,strings);
        if(count!=0){
            getContext().getContentResolver().notifyChange(uri, null);
        }return count;
    }

}
