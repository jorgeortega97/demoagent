package com.wposs.myapplication;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.wposs.myapplication.AdapterList;
import com.wposs.myapplication.content.CompanyProvider;
import com.wposs.myapplication.content.CompanyProviderPackage;
import com.wposs.myapplication.data.data;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ActivityResultLauncher<Intent> resultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(), result ->
                    System.out.println("Entro aquí luego de finalizar"));

    Button button1, button2;
    EditText ident, urls, dns, port_Dwl, port_Upl, frequency, querys, no_query, init, time_out_slide, name_terminal, is_tls, packe;
    RecyclerView recyclerView;
    CardView vista;
    TextView tv_id, tv_url, tv_dns;
    Toolbar toolbar;
    String[] name_package = {"com.wposs.cobranzas", "com.wposs.bancard_lealtad", "com.wpos.bancard"};
    ArrayList<String> listaPackage = new ArrayList<String>();
    Uri uri;
    boolean flag;
    CompanyProvider companyProvider;
    CompanyProviderPackage companyPackage;
    ContentValues valuesConfig = new ContentValues();
    ContentValues valuesPackage = new ContentValues();
    data d = new data();
    String ids;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.app_name));//-->Colocamos el titulo del toolbar "Recargas App"
        toolbar.setSubtitle(getString(R.string.inicio)); //-->Colocamos el subtitulo del toolbar "Iniciar Sesion"
        setSupportActionBar(toolbar);//Barra de herramientas para establecer como la barra de acción de la actividad.
        //-------------------------------------------------------
        button1 = findViewById(R.id.btnIntent);
        vista = findViewById(R.id.cvLista);
        tv_id = findViewById(R.id.tvId);
        tv_url = findViewById(R.id.tvUrl);
        tv_dns = findViewById(R.id.tvDns);
        //packe = findViewById(R.id.tvpacke);
        recyclerView = findViewById(R.id.recyclerView);
        //packe.setEnabled(false);
        button1.setOnClickListener(view -> {
            // intent();
            save();
            listaPackage.clear();
            load();

        });

        vista.setOnClickListener(view -> {
            dialogue();
        });

        if (getCallingActivity() != null) {
            package_search(getCallingActivity().getPackageName());
        }
        companyProvider = new CompanyProvider();
        companyPackage = new CompanyProviderPackage();


        //---------------------------------------------------
        /*listaPackage.add("com.wposs.cobranzas");
        listaPackage.add("com.wposs.bancard_lealtad");
        listaPackage.add("com.wposs.bancard");
        listaPackage.add("com.wposs.bancard");
        listaPackage.add("com.wposs.bancard");
        listaPackage.add("com.wposs.bancard");*/

    }

    private void intent() {
        Intent i = getPackageManager().getLaunchIntentForPackage("com.wposs.bancard");
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //startActivity(i);
        startActivityForResult(i, 0);
    }

    public void save() {
        int rowsUpdated = 0; // Define una variable para contener el número de filas actualizadas
        getPackageManager().toString();
        valuesConfig.put("URL", "http://34.192.135.190/");
        valuesConfig.put("DNS", "http://dev-bancardpcs.wposs.com/");
        valuesConfig.put("PORT_DWL", "7027");
        valuesConfig.put("PORT_UPL", "7025");
        valuesConfig.put("FREQUENCY", "1");
        valuesConfig.put("QUERYS", "1");
        valuesConfig.put("NO_QUERYS", "1.0");
        valuesConfig.put("INIT", "0");
        valuesConfig.put("TIME_OUT_SLIDE", "0");
        valuesConfig.put("NAME_TERMINAL", "fuentes");
        valuesConfig.put("IS_TLS", "0");
        //valuesPackage.put("packageName",getCallingActivity().getPackageName());
        //valuesPackage.put("packageName","com.wposs.bancard-datacenter");
        //valuesPackage.put("status","0");
        valide_package(name_package);
        boolean verify_config = verify_existence_config();
        if (verify_config) {
            uri = getContentResolver().insert(CompanyProvider.CONTENT_URI, valuesConfig);
            Toast.makeText(this, "Registro del config ", Toast.LENGTH_SHORT).show();
        } else {
            rowsUpdated = getContentResolver().update(CompanyProvider.CONTENT_URI, valuesConfig, ids, null);
            Toast.makeText(this, "Actualización del config", Toast.LENGTH_SHORT).show();
        }

    }

    public void load() {
        Cursor cr1 =cursor(1);
        Cursor cr2=cursor(2);
        while (cr1.moveToNext()) {
            tv_id.setText(String.valueOf(cr1.getInt(0)));
            tv_url.setText(cr1.getString(1));
            tv_dns.setText(cr1.getString(2));
            //--------------------------------//
            d.setId(String.valueOf(cr1.getInt(0)));
            d.setUrl(cr1.getString(1));
            d.setDns(cr1.getString(2));
            d.setPort_dwl(cr1.getString(3));
            d.setPort_upl(cr1.getString(4));
            d.setFrequency(cr1.getString(5));
            d.setQuery(cr1.getString(6));
            d.setNo_query(cr1.getString(7));
            d.setInit(cr1.getString(8));
            d.setTime_out_slide(cr1.getString(9));
            d.setName_terminal(cr1.getString(10));
            d.setIs_tls(cr1.getString(11));
            //-------------------------------//
        }
        while (cr2.moveToNext()) {
            listaPackage.add(cr2.getString(1));
        }
        AdapterList adapterPackages = new AdapterList(listaPackage);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapterPackages);
    }

    public boolean verify_existence_config() {
        Cursor cr1 = cursor(1);
        return !cr1.moveToFirst();
    }
    public void package_search(String string) {
         Cursor cr = cursor(2);
        while (cr.moveToNext()) {
            if (cr.getString(1).equalsIgnoreCase(string)) {
                packe.setText(string);
                Toast.makeText(this, "Actualización autorizada", Toast.LENGTH_SHORT).show();
                break;
            }
        }
        packe.setText(null);
        Toast.makeText(this, "Actualización denegada", Toast.LENGTH_SHORT).show();
    }

    public void valide_package(String[] packages) {
        Cursor cr = cursor(2);
        int update=1;
        if(cr.moveToFirst()==false) {//Verificamos si el cursor esta vacio.
            insert_package(packages);
        }else{
            for(String pack: packages) {
                while(cr.moveToNext()) {//Recorremos el cursor
                    if (pack.equalsIgnoreCase(cr.getString(1))) {
                        Toast.makeText(this, "verifico el package Nº" + update, Toast.LENGTH_SHORT).show();
                        flag = false;
                        update++;
                    }
                }
                cr = cursor(2);
                cr.moveToFirst();
                insert_pacakage_unit(pack, flag);
            }
        }
    }
    public void insert_package(String [] packages){
        int accountant = 1;
        for(String pack : packages){
            valuesPackage.put("packageName", pack);
            valuesPackage.put("status", "0");
            uri = getContentResolver().insert(CompanyProviderPackage.CONTENT_URI, valuesPackage);
            Toast.makeText(this, "Registro del package Nº" + accountant, Toast.LENGTH_SHORT).show();
            accountant++;
        }
    }
    public void insert_pacakage_unit(String pack, boolean status){
        int accountant = 1;
        if (status) {
            valuesPackage.put("packageName", pack);
            valuesPackage.put("status", "0");
            uri = getContentResolver().insert(CompanyProviderPackage.CONTENT_URI, valuesPackage);
            Toast.makeText(this, "Registro del package Nº" + accountant, Toast.LENGTH_SHORT).show();
            accountant++;
            flag = true;
        }
    }
    public Cursor cursor(int index) {
        Cursor crs;
        switch (index) {
            case 1:
                crs = getContentResolver().query(CompanyProvider.CONTENT_URI, null, null, null, "_id");
                break;
            case 2:
                crs = getContentResolver().query(CompanyProviderPackage.CONTENT_URI, null, null, null, "_id");
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + index);
        }
        return crs;
    }

    public void dialogue(){
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.popup_informacion);
        ident = dialog.findViewById(R.id.confirm_id);
        urls = dialog.findViewById(R.id.confirm_url);
        dns = dialog.findViewById(R.id.confirm_dns);
        port_Dwl = dialog.findViewById(R.id.confirm_port_dwl);
        port_Upl = dialog.findViewById(R.id.confirm_port_upl);

        frequency = dialog.findViewById(R.id.confirm_frequency);
        querys = dialog.findViewById(R.id.confirm_query);
        no_query = dialog.findViewById(R.id.confirm_no_query);
        init = dialog.findViewById(R.id.confirm_init);
        time_out_slide = dialog.findViewById(R.id.confirm_time_out_slide);
        name_terminal = dialog.findViewById(R.id.confirm_name_terminal);
        is_tls = dialog.findViewById(R.id.confirm_is_tls);
        button2 = dialog.findViewById(R.id.btnconfirmacion);
        //-----------------------------------------------//
        ident.setEnabled(false);
        urls.setEnabled(false);
        dns.setEnabled(false);
        port_Dwl.setEnabled(false);
        port_Upl.setEnabled(false);
        frequency.setEnabled(false);
        querys.setEnabled(false);
        no_query.setEnabled(false);
        init.setEnabled(false);
        time_out_slide.setEnabled(false);
        name_terminal.setEnabled(false);
        is_tls.setEnabled(false);
        //-------------------------------------------------//
        ident.setText(d.getId());
        urls.setText(d.getUrl());
        dns.setText(d.getDns());
        port_Dwl.setText(d.getPort_dwl());
        port_Upl.setText(d.getPort_upl());
        frequency.setText(d.getFrequency());
        querys.setText(d.getQuery());
        no_query.setText(d.getNo_query());
        init.setText(d.getInit());
        time_out_slide.setText(d.getTime_out_slide());
        name_terminal.setText(d.getName_terminal());
        is_tls.setText(d.getIs_tls());
        //--------------------------------------------------//
        dialog.show();
            button2.setOnClickListener(
                    view -> {
                        dialog.dismiss();
                    });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_exit) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}